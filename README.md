# README #

Consists of html and javascript file.

### What is this repository for? ###

* A simple pure javascript infinite image slider

### How do I get set up? ###

* cd to the main project folder
* npm install
* npm run start:dev
* Navigate to localhost:7000 with your browser

### Who do I talk to? ###

* mdtycho@gmail.com