// CAROUSEL OBJECT
function Carousel(containerID) {
    this.container = document.getElementById(containerID) || document.body;
    this.slides = this.container.querySelectorAll('.carousel');
    this.total = this.slides.length - 1;
    this.current = 0;

    // start on slide 1
    this.slide(this.current);
}
// NEXT
Carousel.prototype.next = function() {
    (this.current === this.total) ? this.current = 0: this.current += 1;

    this.slide(this.current);


};
// PREVIOUS
Carousel.prototype.prev = function() {
    (this.current === 0) ? this.current = this.total: this.current -= 1;
    this.slide(this.current);
};

// SELECT SLIDE
Carousel.prototype.slide = function(index) {
    if (index >= 0 && index <= this.total) {
        for (var s = 0; s <= this.total; s++) {
            if (s === index) {
                this.slides[s].style.display = "inline-block";
            } else {
                this.slides[s].style.display = 'none';
            }
        }
    } else {
        alert("Index " + index + " doesn't exist. Available : 0 - " + this.total);
    }
};